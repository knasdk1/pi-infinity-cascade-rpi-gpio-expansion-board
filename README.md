# Pi Infinity Cascade RPI GPIO Expansion Board
![Pi Infinity Cascade RPI GPIO Expansion Board](https://gitlab.com/knasdk1/pi-infinity-cascade-rpi-gpio-expansion-board/raw/master/RPI.jpg)

This is a controller for the *Pi Infinity Cascade RPI GPIO Expansion Board*, written in python for the Raspberry Pi device (also works with the Raspberry Pi Zero). Using the `open()` method, opens the desired ports e.g. `RpiController.open({1: [1], 2: [1]})` opens `row 1 pin 1`, and `row 2 pin 1`.

This is the programmed rows and pins numbering of the board:
```bash
Pins      12345678
Row 4     ........
Row 3     ........
Row 2     ........
Row 1     ........
        .............
        .............
         Extensions
```
## Usage exampe
Either the controller can be imported and usen via a python script, or it can be activated from the bash console.

### Bash
From the console the script `console.py` can trigger the RpiController.

Console help:
```bash
usage: console.py [-h] [-res] [-r ROWS] [-p PINS]

optional arguments:
  -h, --help            show this help message and exit
  -res, --reset         Reset channels
  -r ROWS, --rows ROWS  Choose Rows 1-4 (e.g. 1,2 = row 1 and 2)
  -p PINS, --pins PINS  Choose pins 1-8 (e.g. 13,1 = row 1 pin 1 and 3, row 2 pin 1)
```
Physical example:
```bash
$ python console.py -r 1,2,4 -p 1,123,5
```
The above example is opening `row 1, pin 1`, `row 2, pins 1, 2 and 3` and `row 4, pin 5`. The argument `-r` following comma separated integers 1-4, points the the rows, while `-r` point to the pins for each row separated by comma. 

### Import
Here's an Python example on howto import and use the controller:
```python
from RpiController import RpiController

Rpi = RpiController()

Rpi.open({1: [1, 2, 6, 3, 5], 2: [6]})
```
The above example will open `row 1, pins 1, 2, 6, 3, 5` and `row 2, pin 6`.