from RpiController import RpiController
import argparse
import json
import sys
Rpi = RpiController()

parser = argparse.ArgumentParser()
parser.add_argument("-res", "--reset", action='store_true', help="Reset channels")
parser.add_argument("-r", "--rows", help="Choose Rows 1-4 (e.g. 1,2 = row 1 and 2)")
parser.add_argument("-p", "--pins", help="Choose pins 1-8 (e.g. 13,1 = row 1 pin 1 and 3, row 2 pin 1)")
args = parser.parse_args()

if len(sys.argv) == 1:
    print (parser.print_help())
    exit

if args.reset == 1:
    Rpi.reset()
elif args.rows and args.pins:
    consoleRows = args.rows.split(',')
    consolePins = args.pins.split(',')
    rnp = {}
    for idx, row in enumerate(consoleRows):
        row = int(row)
        rnp[row] = []
        for pins in consolePins[idx]:
            pins = list(pins)
            for pin in pins:
                pin = int(pin)
                rnp[row].append(pin)
    Rpi.open(rnp)