import RPi.GPIO as GPIO
import sys
class RpiController:

    def __init__(self):
        self.pinData = 10
        self.pinLatch = 8
        self.pinClock = 11

        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pinData,  GPIO.OUT)
        GPIO.setup(self.pinLatch, GPIO.OUT)
        GPIO.setup(self.pinClock, GPIO.OUT)

        #    RPI IO Board overview
        #          12345678
        #Row 4     ........
        #Row 3     ........
        #Row 2     ........
        #Row 1     ........
        #        .............
        #        .............

        self.Rows = {
            4: {1:2147483648, 2:1073741824, 3:536870912, 4:268435456, 5:134217728, 6:67108864, 7:33554432, 8:16777216, 0:0},
            3: {1:8388608, 2:4194304, 3:2097152, 4:1048576, 5:524288, 6:262144, 7:131072, 8:65536, 0:0},
            2: {1:32768, 2:16384, 3:8192, 4:4096, 5:2048, 6:1024, 7:512, 8:256, 0:0},
            1: {1: 128, 2:64, 3:32, 4:16, 5:8, 6:4, 7:2, 8:1, 0:0}
        }

    def open(self, rowsAndPins):
        byte = 0
        try:
            for row in rowsAndPins:
                for pin in rowsAndPins[row]:
                    byte += self.Rows[row][pin]
        except:
            error = ''
            if (row < 1 or row > 4):
                error += '\n- Row %s does not exist (must be between 1 and 4).' %(row)
            if (pin < 1 or pin > 8):
                error += '\n- Pin %s does not exist (must be between 1 and 8).' %(pin)
            print "Error.", error
            
        GPIO.output(self.pinLatch, 0)

        try:
            for x in range(32):
                GPIO.output(self.pinData, (byte >> x) & 1)
                self.pulse(self.pinClock)
        except:
            error = ''
            if (row == 4 and pin == 1):
                error += '\n- Pin %s on row %s is unavailable.' %(row, pin)
            else:
                error += '\n- %s' %(sys.exc_info()[0])
            print "Error.", error

        GPIO.output(self.pinLatch, 1)
        
    def pulse(self, pin):
        GPIO.output(pin, 1)
        GPIO.output(pin, 0)

    def blink(self):
        self.open({1: [1,2,3,4,5,6,7,8], 2: [1,2,3,4,5,6,7,8], 3:[1,2,3,4,5,6,7,8], 4:[2,3,4,5,6,7,8]})
        time.sleep(2)
        self.open({1: [0], 2: [0], 3:[0], 4:[0]})
        time.sleep(2)

    def reset(self):
        self.open({1: [0], 2: [0], 3:[0], 4:[0]})
        GPIO.cleanup()
